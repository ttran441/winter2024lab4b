public class VirtualPetApp{
	public static void main(String[] args){
		java.util.Scanner reader = new java.util.Scanner(System.in);
		System.out.println("~~Welcome to VirtualPetApp!~~");
		System.out.println("~~Bear edition~~");
		Bear[] sloth = new Bear[4];
		
		/*
		In this loop, the program asks user to fill the array with required fields,
		asking the user to enter a String name, an int age,
		a double weight and a boolean asking him if he is agressive or not
		*/
		for(int i = 0; i<sloth.length; i++){
			System.out.println("Enter a name to a bear: ");
			String name = reader.nextLine();
			System.out.println("Enter an age: ");
			int age = Integer.parseInt(reader.nextLine()); 
			System.out.println("Enter a weight: ");
			double weight = Double.parseDouble(reader.nextLine());
			System.out.println("Is bear agressive? (true or false) ");
			boolean isAgressive = Boolean.parseBoolean(reader.nextLine());
			sloth[i] = new Bear(name, age, weight, isAgressive);
		}
		
		System.out.println(sloth[sloth.length-1].getName());
		System.out.println(sloth[sloth.length-1].getAge());
		System.out.println(sloth[sloth.length-1].getWeight());
		System.out.println(sloth[sloth.length-1].getIsAgressive());
		
		System.out.print("Please input a new name for the last bear: ");
		String newName = reader.nextLine();
		
		sloth[sloth.length-1].setName(newName);
		
		System.out.println(sloth[sloth.length-1].getName());
		System.out.println(sloth[sloth.length-1].getAge());
		System.out.println(sloth[sloth.length-1].getWeight());
		System.out.println(sloth[sloth.length-1].getIsAgressive());
		
	}
}

