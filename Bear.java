public class Bear{ 
	
	private String name;
	private int age;
	private double weight;
	private boolean isAgressive;
	
	// Constructor for the bear class
	public Bear(String name, int age, double weight, boolean isAgressive) {
		
		this.name = name;
		this.age = age;
		this.weight = weight;
		this.isAgressive = isAgressive;
		
	}
	
	/*
	This method takes no parameters. 
	Depending on certain criterias, it will display different inputs. 
	If the bear's weight >= and age is >= it will display that he isn't 
	capable of doing tricks. If the bear is agressive it will display the text indicating that 
	the bear is not happy. Otherwise it will print that the bear did the trick.
	*/
	public void canDoTricks(){
		if(this.weight >= 200 || this.age >= 15){
			System.out.println(this.name + " doesn't have capabilities to do such things");
		} else if (this.isAgressive == true){ 
			System.out.println(this.name + " is definetely not happy with your actions");
		} else { 
			System.out.println(this.name + " did the coolest trick that bear can do");
		}
		
	}
	
	/*
	This method takes no parameters. 
	Depending on the value of boolean isAgressive, it will print the given name
	of the bear and his reaction.
	*/
	public void canPet(){
		if(this.isAgressive == false){
			System.out.println(this.name + " did let you pet him");
		} else { 
			System.out.println(this.name +" wasn't happy with your presence");
		}
	}
	
	// Getters for private fields
	
	public String getName() {
		
		return this.name;
		
	}
	
	public int getAge() {
		
		return this.age;
		
	}
	
	public double getWeight() {
		
		return this.weight;
		
	}
	
	public boolean getIsAgressive() {
		
		return this.isAgressive;
		
	}
	
	// Setter for one of the private fields
	
	public void setName(String name) {
		
		this.name = name;
		
	}
	
}

